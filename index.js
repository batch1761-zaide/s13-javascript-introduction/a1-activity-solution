// console.log(`Hello World`);

 	let firstName ="John";
 	let lastName = "Smith";
 	let age = 30;

 	const hobbies = ["Biking", "Mountain Climbing", "Swimming"];

 	const workAddress = {
 		housenumber:"32", 
 		street:"Washington", 
 		city:"Lincoln",
 		state:"Nebraska"
 	};

 	let isMarried;

 	printUserInfo(firstName, lastName, age, hobbies, workAddress);

 	function printUserInfo(firstName, lastName, age, hobbies, workAddress){
 		console.log(`First Name: ${firstName}`);
 		console.log(`Last Name: ${lastName}`);
 		console.log(`Age: ${age}`);
 		console.log("Hobbies:");
 		console.log(hobbies);
 		console.log("Work Address:");
 		console.log(workAddress);
 		console.log(`John Smith is ${age} years of age.`);
 		console.log(`This was printed inside of the function`);
 		console.log(hobbies);
 		console.log(`This was printed inside of the function`);
 		console.log(workAddress);

 	}

 	 function returnFunction() {
 		isMarried = true;
 		return isMarried;
 	}

 	console.log(`The value of isMarried is: ${returnFunction()}`);
